\documentclass{report} 
\input{./common/setup.tex}
%\usepackage{picins}
\usepackage{longtable}


\renewcommand{\textfraction}{0.01}
\renewcommand{\topfraction}{1,}
\renewcommand{\bottomfraction}{1.}
%%%%%%%%%%%%%%%%%%%%%%%%    DEFINITIONS    %%%%%%%%%%%%%%%%%%%%%%%%
\def\docfilename{}
\def\title{Eclipse Packaging Project} 
\def\subtitle{Graduation Review Version 1.0.0}
\def\everypagetitle{\textbf{Eclipse Technology Project: EPP\\Graduation Review Version 1.0.0}}
\def\pubdate{\today}
\def\workpackage{}
\def\partners{}
\def\leadpartner{}
\def\configid{}
\def\docclassification{\copyright 2008 by Markus Knauer.\newline
Made available under the Eclipse Public License v1.0.}
\def\istnumber{}
\def\abstract{This document contains the Graduation Review Documentation for the Eclipse Packaging Project (EPP). The 1.0.0 EPP release is scheduled for 2008-06-25 together with the release of Ganymede.}
  
\makeindex

\begin{document}

\input{./common/cover}

%\vspace*{8cm}
%\begin{center}
%This document is intended to be gender-neutral. Nevertheless, we use gender-specific language where expedient to ensure simplicity.
%\end{center}

%\clearpage

%\section*{Delivery Slip}
%\begin{tabular}{|p{2.1cm}|p{4.1cm}|p{2.0cm}|p{2.0cm}|p{3.7cm}|}
%\hline
%                        & Name  & Partner & Date    &  Signature \\ \hline 
%\hline
%\raggedleft From        & Markus Knauer & INO &     &            \\ \hline
%\raggedleft Verified By &       &         &         &            \\ \hline
%\raggedleft Approved By &       &         &         &            \\ \hline
%\end{tabular}

%\section*{Document Log}
%\begin{tabular}{|p{1.0cm}|p{1.9cm}|p{6cm}|p{5.4cm}|}
%\hline
%Version & Date       & Summary of changes & Author  \\ \hline \hline
% 0.1    & 2007-07-05 & First Draft        & Markus Knauer \\  \hline
%\end{tabular}

\tableofcontents
\newpage

\chapter{Overview}

Eclipse is seeing tremendous adoption of its tool and platform offerings. With thousands of downloads every day the Eclipse Platform SDK is the most popular download offering. The SDK includes everything needed for Eclipse plug-in development (Platform, PDE, JDT and Sources). When Eclipse was young, this was almost everything that Eclipse.org had to offer. This situation has changed over the last five years, and Eclipse is offering tools from A (AspectJ) to W (Web Tools Platform) in more than 80 projects.

At the same time, Eclipse is not only serving plug-in developers anymore, but also developers who want to explore Eclipse as a tool for a specific language or domain. Those developers are interested in downloading tools that may differ quite a bit from the Platform SDK download. For instance, developers often don't require the source code or Plug-in Development Environment (PDE), if they are just looking to use Eclipse as a Java IDE. It is possible to extend the Eclipse Platform SDK by using the update manager, but developers generally prefer a single download to get started. This is especially important for developers that are new to Eclipse.

The Eclipse Packaging project aims to provide a set of entry-level downloads for different user profiles. The goal is to improve the usability and out-of-box experience of developers that want to use multiple Eclipse projects.

\section{Scope and goals of the project}

\begin{itemize}
 \item {\bfseries Create entry level downloads based on defined user profiles.} The project defined and created the EPP downloads of Java Developer, Java EE Developer, C/C++ Developer and RCP Developer. These downloads are available from the main Eclipse download page and help users to start with Eclipse.
 \item {\bfseries Provide feedback about the content.} With the integration of the EPP Usage Data Collector it will be possible to collect information about how individuals are using the Eclipse platform. The intent is to use this data to help committers and organizations better understand how developers are using Eclipse.
 \item {\bfseries Help projects to integrate with each other.} With the package centric approach it is possible to build products which contain features of many different Eclipse projects. This leads to an early detection of dependency problems, better integration testing, and a project sturcture that is easier to consume.
 \item {\bfseries Provide a platform that allows the creation of packages (zip/tar downloads) from an update site.} The core technology of the project enables the creation of download packages that are created by bundling Eclipse features from one or multiple Eclipse update sites.
 \item {\bfseries Provide a central build infrastructure for the eclipse.org package builds.} The EPP package builds are running every night and allow early feedback on the content of the release streams (Europa, Ganymede).
 \item {\bfseries Provide an installer} that improves the install experience of new users of Eclipse. (\emph{postponed})
\end{itemize}

Since June 2007, the project delivered packages for all Europa Releases with more than 8,000,000 downloads.

\chapter{Features}

EPP in version 1.0.0 includes

\begin{itemize}
  \item a packaging component that uses the Eclipse Update Manager and the PDE packager
  \item build scripts that are used in the nightly package builds
  \item the UDC (Usage Data Collector) that collects data on an Eclipse client, e.g. an EPP package and sends the data back to the Eclipse Foundation servers.
\end{itemize}

\begin{description}
\item[\texttt{org.eclipse.packaging.core}] core EPP packaging application. The application can be run from the command line and creates the packages defined in the EPP package configuration.
	\begin{enumerate}
    \item Use Eclipse Update Manager to pull features from update sites. Once downloaded into a local repository, the content can be reused in future builds. 
	\item Use the PDE packager to create packages for different platforms and configurations.
	\item Everything driven by a single configuration file that contains additional information to create the website content of the packaging website.
    \end{enumerate}
\item[\texttt{org.eclipse.usagedata.*}] client components of the Eclipse Usage Data Collector. The usage data monitors what is being used and when (timestamp), including
	\begin{itemize}
    \item Loaded bundles
    \item Commands accessed via keyboard shortcuts
    \item Actions invoked via menus or toolbars
    \item Perspective changes
    \item View usage
    \item Editor usage
    \end{itemize}
Captured data is associated with a user through a combination of workstation and workspace ids that are automatically generated by the collector.  This identification is not tied to any personal information about the user. Where possible, the usage data collector also capture the symbolic name and version of the bundle contributing the command/action/perspective/view/editor.
\end{description}

\begin{figure}[!h]
\begin{center}
\includegraphics[width=0.80\textwidth]{img/epp.udc.uploading.eps}
\caption{EPP Usage Data Collector Upload}
\label{fig:epp.udc.preferences}
\end{center} 
\end{figure}

\begin{figure}[!h]
\begin{center}
\includegraphics[width=0.80\textwidth]{img/epp.udc.preview.eps}
\caption{EPP Usage Data Collector Preview}
\label{fig:epp.udc.preferences}
\end{center} 
\end{figure}

% Summarize the major features of this release as well as any other features that have generated significant discussion amongst the community during the development cycle. Compare the features against the Roadmap to understand the project's conformance or divergence.
% Reason: The community will use this release and the ecosystem will build products on top of this release, and both need to know what features were included or excluded.
% References to existing New and Noteworthy documentation is a useful addition to this summary.

\chapter{Non-Code Aspects}
\section{User Documentation}
User documentation has been created for this initial release only in the form of web pages or wiki pages (http://wiki.eclipse.org/index.php/Category:EPP):
\begin{itemize}
  \item How-to build your own package
  \item How-to specify an EPP configuration file
  \item How-to start as a Package Mainainer
  \item Package Testing
  \item Build Infrastructure
\end{itemize}

\section{Localization or Externalization}
EPP is available for the English language; strings are externalized.

There are no plans to provide translated versions.

\chapter{APIs}

\section{EPP Packaging}
The EPP packaging application does not define any extension points. It uses an XML configuration file with a format specified by EPP (http://wiki.eclipse.org/EPP/Configuration\_File\_Format). It contains
\begin{itemize}
  \item name, perspective, product ID
  \item a set of update sites
  \item a set of required features
  \item the base platform archive
  \item a platform specific eclipse.ini file
\end{itemize}
In the future we will add metadata with a package description that can be used on a webpage etc (see http://wiki.eclipse.org/EPP/Packaging\_Site). This metadata is optional.

\section{EPP UDC}
The EPP UDC functionality is split into

\begin{description}
\item[\texttt{org.eclipse.epp.usagedata.gathering}] which defines the \texttt{org.eclipse.epp.usagedata.gathering.monitors} extension point; this extension point is used to plug new monitors to Eclipse. Three monitor implementations are included: PartUsageMonitor, BundleUsageMonitor, CommandUsageMonitor. And it defines the \texttt{org.eclipse.epp.usagedata.listeners.event} extension point; implementators act as receiver of the events generated by the monitors.
\item[\texttt{org.eclipse.epp.usagedata.recording}] which defines the \texttt{org.eclipse.epp.usagedata.recording.uploader} extension point; this extension point allows the creation of different systems to process the data collection. 
\item[\texttt{org.eclipse.epp.usagedata.ui}] defines the UI elements (i.e. preferences pages) and provides an implementation of the uploader extension point that uploads the UDC data to an Eclipse Foundation server.
\end{description}

%Certify that the APIs in this release are Eclipse Quality. The project lead will personally certify that the requirements for quality have been met and/or discuss any deficiences.
%Reason: Eclipse members build commercial tools on top of the extensible frameworks and thus the quality of the APIs is extremely important.


\chapter{Architectural Issues}

\section{EPP Packaging}

The EPP configuration file will be modified to reflect changes in its downstream 'consumers' (package build, website, content management system, installers, \ldots). These changes will contain additional elements and therefore are compatible with the old versions.

\section{EPP Usage Data Collector}

The current implementation of the UDC works in an RCP environment. Future planned enhancements include a UDC that will run unmodified in a RAP environment. Currently, there are no known API changes necessary.

%Summarize the architectural quality of the release. Discuss the intrinsic nature of being extensible embodied by this project. Discuss issues such as unresolved overlap with other projects, unpaid "merge debt" from incorporating various components, and so on.
%Reason: Eclipse members build commercial tools on top of the extensible frameworks and thus the quality of the architecture is important.

\chapter{Tool Usability}

\section{EPP Packaging}

With more than 8,000,000 downloads in the last 10 months, packages generated by EPP have been proven stable.

\begin{figure}[!h]
\begin{center}
\includegraphics[width=0.47\textwidth]{img/epp.download.stats.eps}
\caption{EPP Download Statistics (first 9 months)}
\label{fig:epp.download.stats}
\end{center} 
\end{figure}

The EPP packages are available from the main eclipse.org download page and all community packages from a Drupal driven site.

\section{EPP Usage Data Collector}

Early adopters of the Ganymede Milestone Packages already sent their UDC data to the Eclipse Foundation. The following data was collected on a 14 days period from 2008-02-29 to 2008-03-14: 2,359,688 usage data events were been generated by 453 users (an average of 5,209 events per user):‏

\begin{figure}[!h]
\begin{center}
\includegraphics[width=0.47\textwidth]{img/epp.udc.results.views.eps}
\includegraphics[width=0.47\textwidth]{img/epp.udc.results.editors.eps}
\caption{EPP Usage Data Collector Results (Views and Editors)}
\label{fig:epp.udc.results}
\end{center} 
\end{figure}



%Summarize the usability of the tools. Usability in this sense is about using the tools to solve development problems, not the more academic sense of UI evaluation/testing.
%Reason: Without usable tools, the project will not attract the user community necessary to enable the ecosystem.

\chapter{End-of-Life}

This is an initial release, so there are currently no deprecated or removed APIs or features.

\chapter{Bugzilla}

As of 2008-05-12 there are $136$ bugs in technology/epp. In the end, there will be no blockers left and all open bugs for 1.0.0 will be fixed until the release.

\begin{figure}[!h]
\begin{center}
\includegraphics[width=0.80\textwidth]{img/bugzilla.all.report-2008-05-12.eps}
\caption{EPP Bugzilla Overview}
\label{fig:epp.bugzilla}
\end{center} 
\end{figure}

\chapter{Standards}

\begin{itemize}
  \item EPP uses Java 1.5, compatible with Eclipse 3.3 and 3.4
 \end{itemize}

\chapter{UI Usability}

Only the EPP UDC contains UI elements in form of preferences pages.

\begin{itemize}
  \item Following Eclipse UI usability guidelines
  \item Usability changes based on users' feedback
\end{itemize}

%Summarize the user interface usability and the conformance to the Eclipse User Interface Guidelines. Include section 508 compliance, language pack conformance (does the code support multiple languages), etc. Explain any deviations from the user interface guidelines and standards.
%Reason: The user community is larger than just mouse-wielding, English-speaking, computer jockeys. We need to support that larger community.

\chapter{Schedule}

The plan of the Eclipse Packaging Project is always in parallel with the release train plans, i.e. the Europa and the Ganymede release trains (http://www.eclipse.org/epp/plan.php). 
In the project proposal, EPP defined this tentative plan for the Europa timeframe: 

\begin{table}[!h] 
\begin{tabular}{|p{2.5cm}|p{11cm}|}
\hline
2007-02 0.5M1 & initial definition of entry level download packages for the Eclipse download page   \\ \hline
2007-03 0.5M2 & Basic implementation of EPP platform   \\ \hline
2007-05 0.5M3 & Feature complete for Europa release   \\ \hline
2007-06 0.5 & Beta availabilty for all Eclipse projects and build these packages for Windows, Linux-GTK, MacOSX-Carbon   \\ \hline
\end{tabular}
\caption{EPP 0.5 / Europa Release Dates}
\label{tab:epp.dates}
\end{table}

These scheduled dates have been met and the packages were released together with the Europa Release in June 2007, but there has never been a public available release $0.5$. Due to a lack of resources, a native installer could not be provided in time and is currently postponed.

Within the Ganymede timeframe, EPP follows the milestone and release candidate dates defined for the Ganymede Release as EPP dates (see http://wiki.eclipse.org/Ganymede\_Simultaneous\_Release\#Mile\-stones\_and\_Release\_Candidates) and implements the following features:

\begin{itemize}
  \item Creation of a package eco system where package maintainers can add new packages. 
  \item Development of the EPP Usaged Data Collector and integration in every package.
  \item Delivering milestone builds and nightly build based on the Ganymede Update Site in time; set up of an automated build process and integration with the Ganymatic build.
  \item Re-defininition of the initial packages for the Eclipse download page; package builds for Windows, Linux-GTK, Linux-GTK64, and MacOSX-Carbon
\end{itemize}

\begin{table}[!h] 
\begin{tabular}{|p{2.5cm}|p{11cm}|}
\hline
2008-02-22	& Ganymede M5 (EPP) with UDC  \\ \hline
2008-02-22	& EclipseCon Memory Sticks (EPP)  \\ \hline
2008-02-27	& Europa Winter (EPP)  \\ \hline
2008-02-29	& Europa Winter (Public Access)  \\ \hline
2008-03-31	& EPP new packages must be announced on bugzilla by package maintainer  \\ \hline
2008-04-11	& Ganymede M6 API Freeze (EPP)  \\ \hline
2008-04-11	& EPP new packages and working configuration files available  \\ \hline
2008-05-09	& Ganymede M7 (EPP)  \\ \hline
2008-05-09	& EPP package content feature freeze, package build system feature freeze  \\ \hline
2008-05-20	& EPP release review (UDC)  \\ \hline
2008-05-23	& Ganymede RC1 (EPP)  \\ \hline
2008-05-30	& Ganymede RC2 (EPP)  \\ \hline
2008-06-06	& Ganymede RC3 (EPP)  \\ \hline
2008-06-13	& Ganymede RC4 (EPP)  \\ \hline
2008-06-23	& Ganymede Final Release (EPP)  \\ \hline
\end{tabular}
\caption{EPP 1.0 / Ganymede Release Dates 2008}
\label{tab:epp.dates}
\end{table}

%Discuss the initial schedule and any changes to the schedule over the course of the release, i.e., what the project team achieved. Discuss whether milestones were met or slipped.
%Reason: The community relies on consistent schedules from Eclipse so that projects and products can plan for the correct dependencies.

\chapter{Communities}

\begin{itemize}
  \item Active committers (5) and contributors from 4 partners (INNOOPRACT, Inc., Eclipse Foundation, Instantiations, Xored)
  \item Participation (Talks, BoF) at Eclipse events (EclipseCon 2007, Provisioning Workshop 2008, Eclipse\-Con 2008)
  \item Public conference calls
  \item Developer mailing list with about 200 e-mails, newsgroup with about 150 messages
  \item The Eclipse Packaging Project has been mentioned in many blog postings, other mailing lists (e.g. cross-project-issues-dev)
  \item Participation in the Eclipse Planning Council and in the Eclipse Architecture Council
\end{itemize}

%Summarize the project's development of its three communities. Consider the interactions on bugzilla, the mailing lists, the newsgroups, public conference calls, blogs, PR activities, code camps, conference tutorials, coordinating with other Eclipse projects and other open source projects (Apache, ObjectWeb, etc), ...
%Reason: It is important for Eclipse projects to build a community around the project, not just deliver code for a project. This review item is about the success of building a community.

\chapter{IP Issues}

See IP Log at http://www.eclipse.org/projects/slides/EPP\_eclipse-project-ip-log.csv

\begin{itemize}
  \item Initial code contribution got IP clearance from Eclipse Legal (CQ1395, CQ1536, CQ1898)
  \item All CQs of this release got IP clearance from Eclipse Legal (CQ1913, CQ1914, CQ1915)
  \item External contributions are listed in the IP Log and were submitted via Bugzilla
\end{itemize}

List of committers:

\begin{itemize}
  \item Wayne Beaton - committer since 12/2007
  \item Alexander Kazantsev, initial committer
  \item Markus Knauer, initial committer
  \item Dan Rubel, initial committer
  \item Mark Russell, initial committer
  \item Elias Volanakis, initial committer
\end{itemize}

%As per the Eclipse IP Policy, these steps must be done:
%   1. The project leadership verifies that:
%          * ... that the about files and use licenses are in place as per the Guidelines to Legal Documentation.
%          * ... all contributions (code, documentation, images, etc) has been committed by individuals who are either Members of the Foundation, or have signed the appropriate Committer Agreement. In either case, these are individuals who have signed, and are abiding by, the Eclipse IP Policy.
%          * ... that all significant contributions have been reviewed by the Foundation's legal staff. Include references to the IPZilla numbers of all clearances.
%          * ... that all non-Committer code contributions, including third-party libraries, have been documented in the release and reviewed by the Foundation's legal staff. Include references to the IPZilla numbers of all clearances.
%          * ... that all Contribution Questionnaires have been completed
%          * ... the "provider" field of each feature is set to "Eclipse.org"
%          * ... the "copyright" field of each feature is set to the copyright owner (the Eclipse Foundation is rarely the copyright owner).
%          * ... that any third-party logos or trademarks included in the distribution (icons, help file logos, etc) have been licensed under the EPL.
%          * ... that any fonts or similar third-party images included in the distribution (e.g. in PDF or EPS files) have been licensed under the EPL.
%   2. The PMC provides a Project Log that enumerates:
%         1. every piece of third party software including information on the license
%         2. every major contribution
%         3. the name of every contributor including non-committer contributions via bug fixes with bug number's
%         4. the About files which contain any non-standard terms (e.g., a reference to a license other than the EPL, etc)
%   3. The EMO will validate for (a) and (b) that Contribution Questionnaires have been properly submitted and EMO approvals have been completed.
%   4. A frozen copy of the reviewed-and-approved-by-Eclipse-legal Project Log is part of the Release Review documentation. It can be included in the docuware or as a separate document.

\chapter{Project Plan}

Version 1.1.0 is scheduled for October 2008 (Ganymede Fall). Among other improvements it will include

\begin{itemize}
  \item an update of the Usage Data Collector including a RAP-enabled version
  \item a p2-ification of remaining parts.
\end{itemize}

A detailed plan is not yet available.

\end{document}
